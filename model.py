"""
################################################################################
#                                                                              #
# measures-datascience                                                         #
#                                                                              #
################################################################################
#                                                                              #
# LICENCE INFORMATION                                                          #
#                                                                              #
# This program calculates CO2 emission and cost reduction metrics from a       #
# given set of measures with parametrised inputs                               #
#                                                                              #
#                                                                              #
# 2019 Liam Moore, lmoore90@gmail.com                                          #
#                                                                              #
# This software is released under the terms of the GNU General Public License  #
# version 3 (GPLv3).                                                           #
#                                                                              #
# This program is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# This program is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for     #
# more details.                                                                #
#                                                                              #
# For a copy of the GNU General Public License, see                            #
# <http://www.gnu.org/licenses/>.                                              #
#                                                                              #
################################################################################
"""

import itertools
import re
import os

import sqlalchemy as sa
import pandas as pd
import numpy as np

from collections import namedtuple

_key = namedtuple('_key', 'country measure')

class ModelTable:
    """ associate models with country and measure """
    def __init__(self):
        self.model = {}
    def items(self):
        """emulate dict iterator interface"""
        return self.model.items()
    def add(self, *, country, measure, model):
        self.model[_key(country, measure)] = model
    def get(self, *, country, measure):
        try:
            return self.model[_key(country, measure)]
        except KeyError:
            raise KeyError("no model implemented for:\n"
                           "country: {country}, measure: {measure}")


class BaseModel:
    """Base class for models storing parameters in a SQLite database"""
    def __init__(self,
                 name:'sql table name prefix',
                 db:'string path or sqlalchemy connection',
                 index_col:'column to set as key/index'='index',
                 verbose=False):
        self.name = name
        self._ensure_db_connection(db)
        self.verbose = verbose
        self.index_col = index_col
        try:
            self.load()
        except sa.exc.OperationalError as oe:
            print(f"WARNING: no existing table {self.name} found. Creating empty"
                   " table...")
            self._df = pd.DataFrame(
                columns=[self.index_col]).set_index(self.index_col)
            self._df.name = self.name

    def __getattr__(self, attr):
        if attr in self._df.index:
            return self._df.loc[attr]
        else:
            return super().__getattribute__(attr)

    def _ensure_db_connection(self, db):
        if isinstance(db, sa.engine.base.Connection):
            self.db_connection = db
        elif isinstance(db, str):
            self._ensure_directory(db)
            engine = sa.create_engine(f'sqlite:///{db}',  echo=verbose)
            self.db_connection = engine.connect()
        else:
            raise TypeError("Invalid arg: db should be a string pointing to an SQLite "
                            "database or a sqlalchemy.engine.base.Connection object.")

    def _ensure_directory(self, path):
        directory = os.path.dirname(path)
        if not os.path.exists(directory):
            os.makedirs(directory)

    @property
    def df_inputs(self) -> pd.DataFrame:
        return (self._df[self._df.role == 'input']
                .dropna(axis=0, how='all').dropna(axis=1, how='all'))

    @property
    def df_outputs(self) -> pd.DataFrame:
        return (self._df[self._df.role == 'output']
                .dropna(axis=0, how='all').dropna(axis=1, how='all'))

    @property
    def df_remarks(self) -> pd.DataFrame:
        return (self._df[self._df.role == 'remark']
                .dropna(axis=0, how='all').dropna(axis=1, how='all'))

    @property
    def assumptions(self):
        return list(self.df_remarks.index)

    def add_input(self, name, **kwargs) -> None:
        self._add_entry(name, 'input', **kwargs)
        return self.df_inputs.loc[name]

    def add_output(self, name, **kwargs) -> None:
        self._add_entry(name, 'output', **kwargs)
        return self.df_outputs.loc[name]

    def add_remark(self, name, **kwargs) -> None:
        self._add_entry(name, 'remark', **kwargs)
        return self.df_remarks.loc[name]

    def _add_entry(self, name, role, **kwargs):
        for col, val in kwargs.items():
            self._df.loc[name, col] = val
        self._df.loc[name, 'role'] = role
        if 'date_update' not in kwargs:
            self._df.loc[name, 'date_update'] = pd.Timestamp.now()

    def update_parameter_value(self, parameter, value, property='value') -> None:
        """setter for internal dataframe"""
        if self._df.loc[parameter, 'role'] != 'input':
            raise ValueError(f"{parameter} is not an input parameter and cannot be set!")
        self._df.loc[parameter, property] = value
        if property != 'date_update':
            self._df.loc[parameter, 'date_update'] = pd.Timestamp.now()
        if self.verbose:
            print(f"{parameter} {property} updated to {value}")

    def save(self) -> None:
        self._df.to_sql(name=self.name, con=self.db_connection, if_exists='replace')

    def load(self) -> None:
        self._df = pd.read_sql(self.name, con=self.db_connection)
        self._df.name = self.name
        self._df.set_index(self.index_col, inplace=True)

    @classmethod
    def from_dataframe(cls, df, db,
                       index_col='index',
                       name=None,
                       clear_values=False,
                       verbose=False) -> "Model":
        """alternative constructor for model directly from a dataframe"""
        model = cls.__new__(cls)
        model._df = df.copy()
        _name = df.name
        if clear_values:
            value_cols = [c for c in model._df.columns if 'value' in c]
            for v in value_cols:
                model._df.loc[:, v] = np.nan
        model.verbose = verbose
        model.index_col = index_col
        # derive SQL table name with dataframe name if not supplied
        if name is None:
            name = _name
        model.name = name
        model._ensure_db_connection(db)
        # overwrite modified parameters with dataframe values
        model.save()
        return model

    def calculate_outcome(self, outcome, as_dict=True):
        if hasattr(self, outcome):
            _outcome = getattr(self, outcome)
            if as_dict: # json won't deal with timestamp objects
                _outcome.date_update = str(_outcome.date_update)
                _outcome = dict(_outcome)
            return _outcome
        else:
            raise AttributeError(f"{outcome} not implemented!")

### - derived implementations for specific measures

class SolarPanels(BaseModel):
    """derived class for PV panel measure parameters"""
    def __init__(self, name, db):
        super(SolarPanels, self).__init__(name, db)

    @property
    def n_households_with_pv(self):
        _val = (self.n_households.value * self.frac_E_PV.value)
        return self.add_output(name='n_households_with_pv',
                               value=_val,
                               unit='none')

    @property
    def total_savings_wrt_BAU_2020(self):
        _val = (self.n_households_with_pv.value * self.hours_full_load.value *
                self.power_kWp.value / 1000.)
        return self.add_output(name='total_savings_wrt_BAU_2020',
                               value=_val)

    @property
    def total_emission_reduction_wrt_BAU_2020(self):
        _val = (self.n_households_with_pv.value * self.hours_full_load.value *
                self.power_kWp.value * self.avg_emission_tCO2_per_MWh_BAU_2020.value / 1000.)
        return self.add_output(name='total_emission_reduction_wrt_BAU_2020',
                               value=_val)

    @property
    def avg_investment_cost_per_installation_wrt_BAU(self):
        _val = (self.power_kWp.value * self.eur_investment_per_kWp_wrt_BAU.value)
        return self.add_output(name='avg_investment_cost_per_installation_wrt_BAU',
                               value=_val,
                               unit='eur')

    @property
    def avg_energy_saving_per_installation_wrt_BAU(self):
        _val = (self.power_kWp.value * self.hours_full_load.value)
        return self.add_output(name='avg_energy_saving_per_installation_wrt_BAU',
                               value=_val,
                               unit='kWph')

    @property
    def avg_eur_saving_per_installation_wrt_BAU(self):
        _val = (self.avg_energy_saving_per_installation_wrt_BAU.value *
                self.eur_per_kWh_SEAP_excl_VAT.value)
        return self.add_output(name='avg_eur_saving_per_installation_wrt_BAU',
                               value=_val,
                               unit='eur')

    @property
    def avg_emission_reduction_per_installation_wrt_BAU(self):
        _val = (self.avg_energy_saving_per_installation_wrt_BAU.value *
                self.avg_emission_tCO2_per_MWh_BAU_2020.value) / 1000.
        return self.add_output(name='avg_emission_reduction_per_installation_wrt_BAU',
                               value=_val,
                               unit='tCO2')


class CarToBike(BaseModel):
    """derived class for bike travel measure parameters"""
    def __init__(self, name, db):
        super(CarToBike, self).__init__(name, db)

    @property
    def total_savings_wrt_BAU_2020(self):
        _val = (self.avg_energy_consumption_light_transport_by_2020.value
                * self.km_reduction_by_measure.value)
        _val_2 = _val * 1000. / self.km_reduction_by_measure.value
        return self.add_output(name='total_savings_wrt_BAU_2020',
                               value=_val,
                               value_2=_val_2)

    @property
    def total_savings_fuel_costs_wrt_BAU(self):
        _val = (self.gasoline_power_savings.value_2 * self.price_super_98_eur_per_L.value +
                self.diesel_power_savings.value_2 * self.price_diesel_eur_per_L.value)
        _val_2 = _val / self.km_reduction_by_measure.value
        return self.add_output(name='total_savings_fuel_costs_wrt_BAU',
                               value=_val,
                               value_2=_val_2)

    @property
    def total_investment_cost_wrt_BAU_2020(self):
        _val = 0. # shouldn't this reflect cost difference for car/bike purchase/maintenance?
        return self.add_output(name='total_investment_cost_wrt_BAU_2020',
                               value=_val)#,
                               #unit='eur/km')

    @property
    def total_emission_reduction_wrt_BAU_2020(self):
        _val = (self.fleet_emission_light_transport_by_2020.value *
                self.km_reduction_by_measure.value / 1e6)
        _val_2 = self.fleet_emission_light_transport_by_2020.value / 1e9
        return self.add_output(name='total_emission_reduction_wrt_BAU_2020',
                               value=_val)#,
                               #unit='tons CO2e')

    @property
    def km_tot_passenger_car(self):
        _val = (self.km_tot_numbered_roads_passenger_car.value
                + self.km_tot_unnumbered_roads_passenger_car.value)
        return self.add_output(name='km_tot_passenger_car',
                               value=_val,
                               unit='km')

    @property
    def km_reduction_by_measure(self):
        _val = (self.frac_car_to_bike.value * self.km_tot_passenger_car.value)
        return self.add_output(name='km_reduction_by_measure',
                               value=_val,
                               unit='km')

    @property
    def frac_energy_consumption_from_diesel(self):
        _val = 1/(1 + self.E_total_passenger_cars_gasoline.value
                 / self.E_total_passenger_cars_diesel.value)
        return self.add_output(name='frac_energy_consumption_from_diesel',
                               value=_val,
                               unit='none')

    @property
    def diesel_power_savings(self):
        _val_1 = (self.frac_energy_consumption_from_diesel.value *
                self.total_savings_wrt_BAU_2020.value) # MWh
        _val_2 = ((_val_1 * 3.6) /
            (self.diesel_density.value * self.diesel_energy.value)) # liters
        return self.add_output(name='diesel_power_savings',
                               value=_val_1,
                               unit='MWh',
                               value_2=_val_2,
                               unit_2='l')

    @property
    def gasoline_power_savings(self):
        _val_1 = (self.total_savings_wrt_BAU_2020.value -
                  self.diesel_power_savings.value) # MWh
        _val_2 = ((_val_1 * 3.6) /
            (self.gasoline_density.value * self.gasoline_energy.value)) # liters
        return self.add_output(name='gasoline_power_savings',
                               value=_val_1,
                               unit='MWh',
                               value_2=_val_2,
                               unit_2='l')

class CommunalBiomassBoiler(BaseModel):
    """"""
    def __init__(self, name, db):
        super(CommunalBiomassBoiler, self).__init__(name, db)

    @property
    def investment_cost(self):
        _val = (self.investment_per_m_piping.value *
                self.homes_per_pellet_boiler.value *
                self.piping_length_per_house.value)
        _val_2 = self.investment_per_pellet_boiler.value - _val
        return self.add_output(name='investment_cost',
                               value=_val,
                               unit='eur (pipes)',
                               value_2=_val_2,
                               unit_2='eur (boiler)')

    @property
    def price_difference_wrt_classical_kettle(self):
        _val = (self.investment_per_pellet_boiler.value -
                self.investment_per_classical_boiler.value)
        return self.add_output(name='price_difference_wrt_classical_kettle',
                               value=_val,
                               unit='eur/boiler')

    @property
    def installation_cost_per_mwh(self):
        _val = (self.price_difference_wrt_classical_kettle.value /
                (self.homes_per_pellet_boiler.value *
                 self.avg_hot_water_and_heating_consumption_per_house.value))
        return self.add_output(name='installation_cost_per_mwh',
                               value=_val,
                               unit='eur/MWh')

    @property
    def savings_pellet_wrt_gas(self):
        _val = (self.energy_price_natural_gas.value -
                self.energy_price_biomass_pellet.value)
        _val_2 = _val * self.gas_usage_avoided.value
        _val_3 = _val_2 / self.pellets_per_year.value
        return self.add_output(name='savings_pellet_wrt_gas',
                               value=_val,
                               unit='eur/kWh',
                               value_2=_val_2,
                               unit_2='eur',
                               value_3=_val_3,
                               unit_3='eur/boiler',
                               value_4=_val*1000,
                               unit_4='eur/MWh')

    @property
    def gas_usage_avoided(self):
        _val = (self.pellets_per_year.value * self.homes_per_pellet_boiler.value
                * self.avg_hot_water_and_heating_consumption_per_house.value)
        return self.add_output(name='gas_usage_avoided',
                               value=_val,
                               unit='kWh')

    @property
    def total_co2_emission_reduction(self):
        _val = self.gas_usage_avoided.value / self.pellets_per_year.value
        _val_2 = (self.natural_gas_emission_factor.value *
                  self.gas_usage_avoided.value)
        _val_3 = _val_2 / self.pellets_per_year.value
        _val_4 = _val_2 * 1000 / (self.gas_usage_avoided.value)
        return self.add_output(name='total_co2_emission_reduction',
                               value=_val,
                               unit='kWh/boiler',
                               value_2=_val_2,
                               unit_2='tonCO2',
                               value_3=_val_3,
                               unit_3='tonCO2/boiler',
                               value_4=_val_4,
                               unit_4='tonCO2/MWh')


# check outputs
def test_model(model, attrs):
    for attr in attrs:
        print("output: ", attr)
        print(getattr(model, attr), "\n")
