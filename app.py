"""
################################################################################
#                                                                              #
# measures-datascience                                                         #
#                                                                              #
################################################################################
#                                                                              #
# LICENCE INFORMATION                                                          #
#                                                                              #
# This program calculates CO2 emission and cost reduction metrics from a       #
# given set of measures with parametrised inputs                               #
#                                                                              #
#                                                                              #
# 2019 Liam Moore, lmoore90@gmail.com                                          #
#                                                                              #
# This software is released under the terms of the GNU General Public License  #
# version 3 (GPLv3).                                                           #
#                                                                              #
# This program is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# This program is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for     #
# more details.                                                                #
#                                                                              #
# For a copy of the GNU General Public License, see                            #
# <http://www.gnu.org/licenses/>.                                              #
#                                                                              #
################################################################################
"""

import time
import asyncio

import sqlalchemy as sa
import pandas as pd
import numpy as np
import aiohttp
import argparse

from aiohttp import web
from bs4 import BeautifulSoup

from model import (ModelTable, SolarPanels, CarToBike,
                   CommunalBiomassBoiler, test_model)
from scrape import scrape, SCRAPE_ROUTER
from parse_excel import parse_excel_sheet

### --- global defaults
SHEET_PATH_DEFAULT = './spreadsheets/FPC_Test_Liam.xlsx'
DB_PATH_DEFAULT = '/home/liam/futureproofed/params.db'
VERBOSE = False

OUTPUTS_SOLAR_PANELS = [
    'total_savings_wrt_BAU_2020',
    'total_emission_reduction_wrt_BAU_2020',
    'avg_investment_cost_per_installation_wrt_BAU',
    'avg_energy_saving_per_installation_wrt_BAU',
    'avg_eur_saving_per_installation_wrt_BAU',
    'avg_emission_reduction_per_installation_wrt_BAU'
]

OUTPUTS_CAR_TO_BIKE = [
    'total_savings_wrt_BAU_2020',
    'total_savings_fuel_costs_wrt_BAU',
    'total_investment_cost_wrt_BAU_2020',
    'total_emission_reduction_wrt_BAU_2020',
    'km_tot_passenger_car',
    'km_reduction_by_measure',
    'frac_energy_consumption_from_diesel',
    'diesel_power_savings',
    'gasoline_power_savings'
]

### --- setup script arguments
parser = argparse.ArgumentParser(description=__doc__,
 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# path to database file
parser.add_argument("-db","--db-path", dest="db_path", type=str, default=DB_PATH_DEFAULT,
    help="sql database path path, e.g.: '/home/liam/futureproofed/params.db'")
# verbose mode
parser.add_argument("-v","--verbose", dest="verbose", action="store_true", default=VERBOSE,
    help="print extra stuff")
# path to excel sheet
parser.add_argument("-xlsx","--xlsx-path", dest="xlsx_path", type=str, default=SHEET_PATH_DEFAULT,
    help="path to excel sheet, e.g.: '/home/liam/futureproofed/FPX_Test_Liam.xlsx'")

args = parser.parse_args()

### --- main script

# setup routing table
routes = web.RouteTableDef()
# establish connection to sql database
engine = sa.create_engine(f'sqlite:///{args.db_path}', echo=args.verbose)
con = engine.connect()
# container for models
models = ModelTable()

# scrape belgium info from excel sheet
df_pv, df_fiets = parse_excel_sheet(args.xlsx_path)
# create models linked to sql database

# for belgium, create & update with input parameter values from sheet
models.add(
    country='belgium',
    measure='solar_panels',
    model=SolarPanels.from_dataframe(df_pv, con, name='solar_panels_belgium')
)
models.add(
    country='belgium',
    measure='car_to_bike',
    model=CarToBike.from_dataframe(df_fiets, con, name='car_to_bike_belgium')
)

# for spain, reuse parameter metadata, clearing values
models.add(
    country='spain',
    measure='solar_panels',
    model=SolarPanels.from_dataframe(df_pv, con, name='solar_panels_spain', clear_values=True)
)
models.add(
    country='spain',
    measure='car_to_bike',
    model=CarToBike.from_dataframe(df_fiets, con, name='car_to_bike_spain', clear_values=True)
)

# scrape to add example input param values for spain
models.get(country='spain', measure='solar_panels').add_input(
    'n_households',
    **scrape(parameter='n_households', country='spain')
)
# choose a different frac from PV
models.get(country='spain', measure='solar_panels').add_input(
    'frac_E_PV',
    value=0.15
)
models.get(country='spain', measure='car_to_bike').add_input(
    'km_tot_numbered_roads_passenger_car',
    **scrape(parameter='km_tot_numbered_roads_passenger_car', country='spain')
)

bm = CommunalBiomassBoiler(name='communal_biomass_boilers_belgium', db=con)
bm.load()

# add biomass pellet boiler
models.add(
    country='belgium',
    measure='biomass_boiler',
    model=bm
)

#print(models.get(country='spain', measure='solar_panels').df_inputs)
#print("\n\n")
#print(models.get(country='spain', measure='car_to_bike').df_inputs)

@routes.get(r'/outcome/{measure}/{country}/{parameter}')
async def get_handler(request, error=''):
    try:
        measure = request.match_info['measure']
        country = request.match_info['country']
        parameter = request.match_info['parameter']
        m = models.get(country=country, measure=measure)
        result = m.calculate_outcome(parameter)
        result.update(assumptions=m.assumptions)
    except Exception as e:
        result = {}
        error = e.args[0]
    return web.json_response({'result':result, 'error':error})


app = web.Application()
app.add_routes(routes)
web.run_app(app)
