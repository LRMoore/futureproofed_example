# futureproofed database, model, scraper etc

## installation instructions

Install all dependencies in a virtual environment with pipenv (python >=3.6):

```
pip install --user pipenv
pipenv install
```

This will read the Pipfile and normalise all your versions to the same as mine.

Now activate the virtualenv with:

```
pipenv shell
```

And it should run!

## description

* *parse_excel.py* parses excel sheet to produce two sets of
 parameters (inputs and outputs) as pandas dataframes, along with assumptions
* *scrape.py* contains a registry of scraper functions to
 e.g. scrape wikipedia for house numbers
* *model.py* contains a model class which interfaces dataframes to an SQL database
 along with providing parameter addition/editing/save/load functionality. derived
 classes implement formulas to get outputs of solar panel/bicycle measures from
 respective inputs.
* *app.py* is the main script, constructing a pair of models for each country,
 populating their params by parsing excel for belgium and scraping a subset of
 parameters from the interwebs for spain. this then runs a local server handling
 http GET requests
* *test_app.py* throws out a few requests to query different outcomes for each model
 _when the server is running_ (so use another shell)

## usage

start the server locally:

```bash
python app.py
```

send off a few test requests and view results on terminal:

(in another shell)

```bash
python test_app.py
```

in general, endpoints have the format:

```
/outcome/{measure}/{country}/{parameter}
```

for example:

```
'/outcome/solar_panels/belgium/total_savings_wrt_BAU_2020'
```

not all outcomes are implemented for spain

## help

for help configuring script args:

```bash
python app.py --help
```

## comments/limitations

I implemented one real scraper for spain for proof of principle and just punched in a few
numbers from elsewhere enough to calculate some simple derived parameters (since I already
spent quite a bit more time than intended).

Feel free to contact me if you have any trouble running this: lmoore90@gmail.com
