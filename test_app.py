"""
################################################################################
#                                                                              #
# measures-datascience                                                         #
#                                                                              #
################################################################################
#                                                                              #
# LICENCE INFORMATION                                                          #
#                                                                              #
# This program calculates CO2 emission and cost reduction metrics from a       #
# given set of measures with parametrised inputs                               #
#                                                                              #
#                                                                              #
# 2019 Liam Moore, lmoore90@gmail.com                                          #
#                                                                              #
# This software is released under the terms of the GNU General Public License  #
# version 3 (GPLv3).                                                           #
#                                                                              #
# This program is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# This program is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for     #
# more details.                                                                #
#                                                                              #
# For a copy of the GNU General Public License, see                            #
# <http://www.gnu.org/licenses/>.                                              #
#                                                                              #
################################################################################
"""

import asyncio
import aiohttp

async def aiohttp_get(url, session=None):
    new_sesh = session is None
    if new_sesh:
        session = aiohttp.ClientSession()
    async with session.get(url) as response:
        try:
            if response.status == 200:
                ctype = response.headers.get('Content-type','').lower().split(';')[0]
                data = None
                if False:#'json' in ctype or 'text/html' in ctype:
                    data = await response.json(content_type=ctype)
                else:
                    data = await response.text()
                return data
            elif response.status == 404:
                raise aiohttp.web.HTTPNotFound()
            else:
                raise HttpProcessingError(code=response.status,
                                          message=response.reason,
                                          headers=response.headers)
        except:
            raise
        finally:
            if new_sesh:
                await session.close()



TEST_ENDPOINTS = [
    '/outcome/solar_panels/belgium/total_savings_wrt_BAU_2020',
    '/outcome/solar_panels/belgium/total_emission_reduction_wrt_BAU_2020',
    '/outcome/car_to_bike/belgium/total_savings_fuel_costs_wrt_BAU',
    '/outcome/solar_panels/spain/n_households_with_pv',
    '/outcome/car_to_bike/spain/km_tot_passenger_car',

]

async def main():
    base_url = 'http://0.0.0.0:8080'
    for ep in TEST_ENDPOINTS:
        url = base_url + ep
        print("*")
        print("REQUEST: ", url)
        data = await aiohttp_get(url)
        print("RESPONSE: ", data)
        print("*")


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
