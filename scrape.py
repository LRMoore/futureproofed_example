"""
################################################################################
#                                                                              #
# measures-datascience                                                         #
#                                                                              #
################################################################################
#                                                                              #
# LICENCE INFORMATION                                                          #
#                                                                              #
# This program calculates CO2 emission and cost reduction metrics from a       #
# given set of measures with parametrised inputs                               #
#                                                                              #
#                                                                              #
# 2019 Liam Moore, lmoore90@gmail.com                                          #
#                                                                              #
# This software is released under the terms of the GNU General Public License  #
# version 3 (GPLv3).                                                           #
#                                                                              #
# This program is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# This program is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for     #
# more details.                                                                #
#                                                                              #
# For a copy of the GNU General Public License, see                            #
# <http://www.gnu.org/licenses/>.                                              #
#                                                                              #
################################################################################
"""

import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

SCRAPE_ROUTER = {}

def scraper(country, parameter):
    """decorator for registering specific scrapers"""
    def decorate(func):
        print(f'registering scraper for country: {country} and parameter: {parameter}')
        if not SCRAPE_ROUTER.get(country):
            SCRAPE_ROUTER.update({country:{}})
        SCRAPE_ROUTER[country].update({parameter:func})
        return func
    return decorate

@scraper(country='*', parameter='n_households')
def _(parameter, country, *args, **kwargs):
    """pull from wikipedia"""
    url = 'https://en.wikipedia.org/wiki/List_of_countries_by_number_of_households'
    print(f"GET: {url}")
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    l = []
    table = soup.find('table')
    table_rows = table.find_all('tr')

    for tr in table_rows:
        td = tr.find_all('td')
        row = [tr.text for tr in td]
        l.append(row)
    _df = pd.DataFrame(l, columns=["country",
                                   "population",
                                   "n_households",
                                   "avg_household_size",
                                   "date_update"]).dropna()
    _df.loc[:,'country'] = _df['country'].str.lower().str.lstrip('\xa0')
    _df.loc[:, 'n_households'] = _df['n_households'].str.replace(',','')
    _df.loc[:, 'n_households'] = _df['n_households'].astype(np.int64)
    _df.loc[:, 'date_update'] = pd.to_datetime(_df.date_update.str[:4])
    row = _df[_df['country'] == country].iloc[0]
    return {'value' : row.n_households, 'date_update':row.date_update}

def scrape(parameter, country, registry=SCRAPE_ROUTER):
    _d = registry.get(country, {})
    _d.update(registry.get('*'))
    _f = _d.get(parameter)
    if _f is not None:
        return _f(parameter=parameter, country=country)
    else:
        raise NotImplementedError(f"no scraper registered for: {parameter} in {country}")

@scraper(country='spain', parameter='km_tot_numbered_roads_passenger_car')
def _(parameter, country, *args, **kwargs):
    """ripped directly"""
    src_url = "https://tradingeconomics.com/spain/roads-total-network-km-wb-data.html"
    return {'value':667064, 'date_update':pd.Timestamp('2007')}

@scraper(country='spain', parameter='km_tot_unnumbered_roads_passenger_car')
def _(parameter, country, *args, **kwargs):
    """ripped directly"""
    src_url = "https://tradingeconomics.com/spain/roads-total-network-km-wb-data.html"
    return {'value':0, 'date_update':pd.Timestamp('2007')}
