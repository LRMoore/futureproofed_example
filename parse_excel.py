"""
################################################################################
#                                                                              #
# measures-datascience                                                         #
#                                                                              #
################################################################################
#                                                                              #
# LICENCE INFORMATION                                                          #
#                                                                              #
# This program calculates CO2 emission and cost reduction metrics from a       #
# given set of measures with parametrised inputs                               #
#                                                                              #
#                                                                              #
# 2019 Liam Moore, lmoore90@gmail.com                                          #
#                                                                              #
# This software is released under the terms of the GNU General Public License  #
# version 3 (GPLv3).                                                           #
#                                                                              #
# This program is free software: you can redistribute it and/or modify it      #
# under the terms of the GNU General Public License as published by the Free   #
# Software Foundation, either version 3 of the License, or (at your option)    #
# any later version.                                                           #
#                                                                              #
# This program is distributed in the hope that it will be useful, but WITHOUT  #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for     #
# more details.                                                                #
#                                                                              #
# For a copy of the GNU General Public License, see                            #
# <http://www.gnu.org/licenses/>.                                              #
#                                                                              #
################################################################################
"""
import pandas as pd
import numpy as np
import re

NAME_SUBSTITUTIONS = {
    ' ' : '_',
    '(' : '',
    ')' : '',
    '<' : 'lt',
    '>' : 'gt',
    '=' : 'eq'
}

def process_string(string, subs=NAME_SUBSTITUTIONS):
    """perform sequence of character substitutions"""
    for s1, s2 in subs.items():
        string = string.replace(s1, s2)
    return string

def parse_sheet_inputs(df, rename_input_params=None, process_name=process_string):
    """parse left hand side of xls sheet into a dataframe of
       input parameters, leaving right hand side untouched
    """
    # assign properties to each data member from relevant cells
    name = process_name(df.iloc[0, 5])
    # collect input parameters/metadata from named columns
    df = df.reset_index()
    param_columns = [c for c in df.columns if 'unnamed' not in c.lower()]
    rename_mapping = {c:re.sub('\s', '_', c.lower().rstrip(' '))
                      for c in param_columns}
    # split off dataframe of input parameters to model
    df_input_param = df[param_columns].rename(rename_mapping, axis='columns')
    df_rest = df[[c for c in df.columns if c not in param_columns]]
    # get most recent update date
    dt_updated = pd.Timestamp(
        df_input_param.iloc[0].unique_data_code.split()[-1])
    # drop nans
    df_input_param = (df_input_param.dropna(axis=0, how='all').dropna(axis=1, how='all')
                      .drop(0).reset_index(drop=True))
    # correct types/values
    df_input_param['date_update'] = dt_updated
    df_input_param['data_column'] = df_input_param['data_column'].astype(np.int64)
    df_input_param['unit'] = df_input_param['unit'].replace(np.NaN, 'none')
    # clarify names
    df_input_param.rename({'parameter':'value', 'unique_data_code':'parameter'},
        axis='columns', inplace=True)
    # mark input parameters
    df_input_param['role'] = 'input'
    if rename_input_params is not None:
        df_input_param.parameter = np.array(rename_input_params)
    df_input_param = df_input_param.set_index('parameter')
    df_input_param.name = name
    # output df
    return df_input_param, df_rest

def parse_sheet_outputs(df, rename_output_params=None):
    """extract two dataframes containing impact metrics and units,
       and remarks from right sheet of excel
    """
    c0, *crest = df.columns
    # isolate rows associated with measures, and with assumptions/remarks, throw away empties
    ix_maatregelen = df.loc[df[c0] == 'Impact maatregel t.o.v. BAU in 2020'].index[0]
    ix_bemerkingen = df.loc[df[c0] == 'Bemerkingen'].index[0]
    df_maatregelen = (df.iloc[ix_maatregelen+1:ix_bemerkingen]
        .dropna(how='all', axis=0).dropna(how='all', axis=1))
    df_bemerkingen = (df.iloc[ix_bemerkingen+1:]
        .dropna(how='all', axis=0).dropna(how='all', axis=1))
    # throw out empty rows/columns
    #for _df in (df_maatregelen, df_bemerkingen):
    #    for ax in (0,1):
    #        _df.dropna(how='all', axis=ax, inplace=True)
    # determine number of units used to express measure impact
    unit_suffixes = range(int((len(df_maatregelen.columns)-1)/2))
    def suffx(s):
        return '' if s == 0 else f'_{s+1}'
    unit_cols = [[f'value{suffx(s)}', f'unit{suffx(s)}'] for s in unit_suffixes]
    unit_cols = [item for sublist in unit_cols for item in sublist]
    df_maatregelen.columns = ['impact_metric'] + unit_cols
    df_bemerkingen.columns = ['remarks']
    df_maatregelen.loc[:,'role'] = 'output'
    df_bemerkingen.loc[:,'role'] = 'remark'
    df_maatregelen = df_maatregelen.loc[df_maatregelen['impact_metric'].dropna().index]
    if rename_output_params is not None:
        df_maatregelen.impact_metric = np.array(rename_output_params)
    # set indices
    df_maatregelen.set_index('impact_metric', inplace=True, drop=True)
    df_bemerkingen.set_index('remarks', inplace=True, drop=True)
    return df_maatregelen[df_maatregelen.index != np.nan], df_bemerkingen

# abridged column names
short_par_names_pv_in = [
    'frac_E_PV',
    'n_households',
    'power_kWp',
    'hours_full_load',
    'yr_lifespan',
    'eur_investment_per_kWp_wrt_BAU',
    'discount_rate_E',
    'eur_per_kWh_SEAP_excl_VAT',
    'avg_emission_tCO2_per_MWh_BAU_2020']

short_par_names_fiets_in = [
    'frac_car_to_bike',
    'km_tot_numbered_roads_passenger_car',
    'km_tot_unnumbered_roads_passenger_car',
    'fleet_emission_light_transport_by_2020',
    'avg_energy_consumption_light_transport_by_2020',
    'price_diesel_eur_per_L',
    'price_super_98_eur_per_L',
    'E_total_passenger_cars_diesel',
    'E_total_passenger_cars_gasoline',
    'diesel_density',
    'diesel_energy',
    'gasoline_density',
    'gasoline_energy'
]

short_par_names_pv_out = [
    'total_savings_wrt_BAU_2020',
    'total_emission_reduction_wrt_BAU_2020'
]

short_par_names_fiets_out = [
    'total_savings_wrt_BAU_2020',
    'total_savings_fuel_costs_wrt_BAU',
    'total_investment_cost_wrt_BAU_2020',
    'total_emission_reduction_wrt_BAU_2020'
]

def parse_excel_sheet(sheet:'path to sheet',
                      rename_input_params_pv=short_par_names_pv_in,
                      rename_output_params_pv=short_par_names_pv_out,
                      rename_input_params_fiets=short_par_names_fiets_in,
                      rename_output_params_fiets=short_par_names_fiets_out):
    """Parse .xls file and produce two dataframes (one for each measure) with which
       to construct model and database
    """
    raw_df = pd.read_excel(sheet, index_col=0)
    df_pv, df_fiets = raw_df.iloc[:18], raw_df.iloc[18:]
    # parse input parameters from left sheet
    df_inputs_pv, df_rest_pv = parse_sheet_inputs(df_pv, rename_input_params_pv)
    df_inputs_fiets, df_rest_fiets = parse_sheet_inputs(df_fiets, rename_input_params_fiets)
    # parse remarks and impact parameters from right sheet
    df_impact_pv, df_remarks_pv = parse_sheet_outputs(df_rest_pv, rename_output_params_pv)
    df_impact_fiets, df_remarks_fiets = parse_sheet_outputs(df_rest_fiets, rename_output_params_fiets)
    # master dataframes for each measure
    df_pv = pd.concat([df_inputs_pv, df_impact_pv, df_remarks_pv], sort=False)
    df_fiets = pd.concat([df_inputs_fiets, df_impact_fiets, df_remarks_fiets], sort=False)
    # set names
    df_pv.name = df_inputs_pv.name
    df_fiets.name = df_inputs_fiets.name
    return df_pv, df_fiets
